<?php

class CI_Object {
    /**
     * 
     * permite que libraries acessem propriedades e métodos do Code Igniter.
     * 
     * @param string $key
     */
    public function __get($key) {
        return get_instance()->$key;
    }
}