<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function show($conteudo, $menu = true) {
        $html  = $this->load->view('common/cabecalho', NULL, true);
        if($menu) {
            $html .= $this->load->view('common/navbar', NULL, true);
        }
        $html .= $conteudo;
        $html .= $this->load->view('common/rodape', NULL, true);
        echo $html;
    }
}

?>